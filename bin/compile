#!/usr/bin/env bash
# bin/compile <build-dir> <cache-dir>

# fail fast
set -e

# debug
# set -x

shopt -s extglob

function error() {
  echo " !     $*" >&2
  exit 1
}

function indent() {
  c='s/^/       /'
  case $(uname) in
    Darwin) sed -l "$c";;
    *)      sed -u "$c";;
  esac
}

function read_var() {
  echo $(head -n 1 $1)
}

# clean up leaking environment
unset GIT_DIR

# parse and derive params
BUILD_DIR=$1
CACHE_DIR="$2/vendor"
LP_DIR=`cd $(dirname $0); cd ..; pwd`
BUILDPACK_DIR="$(dirname $(dirname $0))"

# fix STACK variable if unset
if [ -z "${STACK}" ];
then
  STACK="cedar-14"
fi

# config

# read in R version if file exists
if [[ -f $BUILD_DIR/.r-version ]]; then
  R_VERSION=`read_var $BUILD_DIR/.r-version`
else
  R_VERSION="3.2.2"
fi

# read in buildback binary version if file exists
if [[ -f $BUILD_DIR/.r-buildpack-version ]]; then
  BUILD_PACK_VERSION=`read_var $BUILD_DIR/.r-buildpack-version`
else
  BUILD_PACK_VERSION="20151031-1711"
fi

S3_BUCKET="heroku-buildpack-r"
#R_BINARIES="http://${S3_BUCKET}.s3.amazonaws.com/${STACK}/R-${R_VERSION}-binaries-${BUILD_PACK_VERSION}.tar.gz"
R_BINARIES="http://heroku-buildpack-r.s3.amazonaws.com/cedar-14/R-3.2.3-binaries-20151214-2343.tar.gz"
VENDOR_DIR="$BUILD_DIR/vendor"
KNIME_DIR="$VENDOR_DIR/knime"
CRAN_MIRROR="http://cran.revolutionanalytics.com"
mkdir -p $CACHE_DIR
REPOSITORY_ROOT="download.pivotal.io.s3.amazonaws.com"
JDK_VERSION="1.8.0"
#JDK="openjdk-${JDK_VERSION}"
JDK="jdk${JDK_VERSION}"
PLATFORM="lucid"
ARCH="x86_64"
#JDK_URL="http://${REPOSITORY_ROOT}/openjdk/${PLATFORM}/${ARCH}/${JDK}.tar.gz"
JDK_URL="http://download.pivotal.io.s3.amazonaws.com/openjdk-jdk/centos6/x86_64/openjdk-1.8.0.tar.gz"
KNIME_URL="http://bdp-knime-installation-minimal.apps.bosch-iot-cloud.com/dist/knime_2.12.0.linux.gtk.x86_64.tar.gz"
PYTHON_URL="https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda2-2.4.0-Linux-x86_64.sh"
echo "JDK_URL: $JDK_URL" | indent
JAVA_HOME_TEMP="$VENDOR_DIR/$JDK"
#: ${JAVA_HOME=}
#: ${JAVA_CPPFLAGS=~autodetect~}
#: ${JAVA_LD_LIBRARY_PATH=~autodetect~}
#: ${JAVA_LIBS=~autodetect~}

echo "Downloading and unpacking $JDK into '$VENDOR_DIR'" | indent
#package_download "${JDK_URL}" "${VENDOR_DIR}"
curl ${JDK_URL} -s -o - | tar xzf - -C $BUILD_DIR
#wget "${JDK_URL}" --no-check-certificate
#mkdir -p $JAVA_HOME_TEMP
#mv jdk-7u51-linux-x64.tar.gz "${JAVA_HOME_TEMP}"
#tar xzf "${JAVA_HOME_TEMP}/jdk-7u51-linux-x64.tar.gz"
#rm "${JAVA_HOME_TEMP}/jdk-7u51-linux-x64.tar.gz"

rm -rf $BUILD_DIR/src.zip

echo "JAVA_HOME_TEMP: $JAVA_HOME_TEMP" | indent
#export JAVA_HOME="/app/vendor/${JDK}"
#mkdir -p /app/openjdk-1.8.0
#cp -R $VENDOR_DIR/openjdk-1.8.0/* /app/openjdk-1.8.0
export JAVA_HOME=$VENDOR_DIR
echo "JAVA_HOME: $JAVA_HOME" | indent
export JAVA_CPPFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
echo "JAVA_CPPFLAGS: $JAVA_CPPFLAGS" | indent

echo "Downloading and unpacking Python binaries" | indent
#curl -o $BUILD_DIR/anaconda_python $PYTHON_URL | bash -b -p $BUILD_DIR/anaconda_pythoninstall/
#comment start
curl -s $PYTHON_URL > $BUILD_DIR/tmp.sh
chmod +x $BUILD_DIR/tmp.sh

bash $BUILD_DIR/tmp.sh -b -p $BUILD_DIR/anacondaPython

rm -rf $BUILD_DIR/tmp.sh

rm -rf $BUILD_DIR/anaconda_python/tests/*
rm -rf $BUILD_DIR/demo/*

chmod +x $BUILD_DIR/anacondaPython/bin/pip
$BUILD_DIR/anacondaPython/bin/pip install protobuf

#mkdir -p $JAVA_HOME
#echo "Copying vendored ${JAVA_HOME_TEMP} files into ${JAVA_HOME}" | indent
#cp -R $JAVA_HOME_TEMP/* $JAVA_HOME

#if [ -f "/usr/bin/java" ]; then
#     echo "Java found on path" | indent
#else
    #ln -fs $JAVA_HOME/bin/java /usr/bin/java
#fi
# vendor R into the slug
echo "Vendoring R $R_VERSION for $STACK stack ($BUILD_PACK_VERSION)" | indent

# download and unpack binaries
echo "Downloading and unpacking R binaries ($R_BINARIES)" | indent
curl $R_BINARIES -s -o - | tar xzf - -C $BUILD_DIR

echo "Downloading and unpacking Knime binaries" | indent
curl $KNIME_URL -s -o - | tar xzf - -C $BUILD_DIR

#bash $BUILD_DIR/anaconda_python/bin/pip install numpy
        #system 'anaconda_python/bin/pip install pandas'
        #system 'anaconda_python/bin/pip install protobuf'
# need to copy the binaries to /app/vendor so that R works when compiling packages

mkdir -p /app/.apt
cp -R $VENDOR_DIR/.apt/* /app/.apt

mkdir -p /app/vendor/R
cp -R $VENDOR_DIR/R/* /app/vendor/R

mkdir -p /app/vendor/KNIME
cp -R $VENDOR_DIR/* /app/vendor/KNIME

# needed for compiling packages
export PATH="/app/vendor/R/bin:/app/.apt/usr/bin:/app/bin:/usr/ruby1.9.2/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
export R_INCLUDE="/app/vendor/R/lib64/R/include"
export LD_LIBRARY_PATH="/app/.apt/usr/lib/libblas:/app/.apt/usr/lib/lapack:/app/.apt/usr/lib/x86_64-linux-gnu:/app/.apt/usr/lib/i386-linux-gnu:/app/.apt/usr/lib:$LD_LIBRARY_PATH"
export LIBRARY_PATH="/app/.apt/usr/lib/x86_64-linux-gnu:/app/.apt/usr/lib/i386-linux-gnu:/app/.apt/usr/lib:$LIBRARY_PATH"
export INCLUDE_PATH="/app/.apt/usr/include:$INCLUDE_PATH"
export CPATH="$INCLUDE_PATH"
export CPPPATH="$INCLUDE_PATH"
export PKG_CONFIG_PATH="/app/.apt/usr/lib/x86_64-linux-gnu/pkgconfig:/app/.apt/usr/lib/i386-linux-gnu/pkgconfig:/app/.apt/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
export LDFLAGS="-L/app/.apt/usr/lib/libblas -L/app/.apt/usr/lib/lapack $LDFLAGS"

# copy over environment
mkdir -p $BUILD_DIR/.profile.d
cp "$BUILDPACK_DIR/bin/r_environment.sh" $BUILD_DIR/.profile.d/r_environment.sh

# prevent warnings when building packages
mkdir -p /app/vendor/R/lib64/R/doc/html
touch /app/vendor/R/lib64/R/doc/html/R.css

# install dependencies from CRAN
echo "Executing init.r script" | indent

# set the CRAN mirror and run the init.r program
/app/vendor/R/bin/R -s <<RPROG > indent
  Sys.setenv(BUILD_DIR="$BUILD_DIR")
  Sys.setenv(JAVA_HOME="$JAVA_HOME")
  Sys.setenv(JAVA_CPPFLAGS="$JAVA_CPPFLAGS")
  setwd("$BUILD_DIR")
  r <- getOption("repos");
  r["CRAN"] <- "$CRAN_MIRROR";
  options(repos=r);
  `cat $BUILD_DIR/init.r`
RPROG

ln -s $VENDOR_DIR/R/bin/R $BUILD_DIR/bin/R

echo "R $R_VERSION successfully installed" | indent

# need to copy binaries back so that any
# installed packages are included in the slug
rm -rf $VENDOR_DIR/R
mkdir -p $VENDOR_DIR/R
cp -R /app/vendor/R/* $VENDOR_DIR/R

#mkdir -p $VENDOR_DIR/KNIME
#cp -R /app/vendor/knime_2.12.0 $VENDOR_DIR/KNIME
#rm -rf $VENDOR_DIR/knime_2.12.0
#R CMD Rserve --vanilla
#telnet localhost 6311
#R CMD javareconf